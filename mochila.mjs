import fs from "fs";
import random from "random";
import { createCanvas } from "canvas";
import Chart from "chart.js";

function knapsackDynamic(value, weight, capacity) {
  const startTime = process.hrtime();
  const n = value.length;
  const dp = Array.from(Array(n + 1), () => Array(capacity + 1).fill(0));

  for (let i = 1; i <= n; i++) {
    for (let w = 1; w <= capacity; w++) {
      if (weight[i - 1] <= w) {
        dp[i][w] = Math.max(
          dp[i - 1][w],
          dp[i - 1][w - weight[i - 1]] + value[i - 1]
        );
      } else {
        dp[i][w] = dp[i - 1][w];
      }
    }
  }

  const endTime = process.hrtime(startTime);
  const dynamicTime = endTime[0] * 1000 + endTime[1] / 1000000;
  return { value: dp[n][capacity], time: dynamicTime };
}

function graspKnapsack(value, weight, capacity, maxIterations, alpha) {
  const startTime = process.hrtime();
  let bestValue = 0;
  const n = value.length;

  for (let iteration = 0; iteration < maxIterations; iteration++) {
    let remainingCapacity = capacity;
    const solution = [];
    let currentValue = 0;

    while (remainingCapacity > 0) {
      const candidates = [];

      for (let i = 0; i < n; i++) {
        if (!solution.includes(i) && weight[i] <= remainingCapacity) {
          candidates.push(i);
        }
      }

      if (candidates.length === 0) {
        break;
      }

      const randomIndex = Math.floor(random.float() * candidates.length);
      const chosenItem = candidates[randomIndex];

      if (random.float() <= alpha) {
        solution.push(chosenItem);
        remainingCapacity -= weight[chosenItem];
        currentValue += value[chosenItem];
      }
    }

    if (currentValue > bestValue) {
      bestValue = currentValue;
    }
  }

  const endTime = process.hrtime(startTime);
  const graspTime = endTime[0] * 1000 + endTime[1] / 1000000;
  return { value: bestValue, time: graspTime };
}

function generateValueComparisonChart(
  outputMaxValuesDynamic,
  outputMaxValuesGRASP
) {
  const canvas = createCanvas(600, 400);
  const ctx = canvas.getContext("2d");

  const chart = new Chart(ctx, {
    type: "bar",
    data: {
      labels: [...Array(16).keys()].map((i) => `Test ${i + 1}`),
      datasets: [
        {
          label: "Dynamic Programming",
          data: outputMaxValuesDynamic.map((result) => result.value),
          backgroundColor: "rgba(75, 192, 192, 0.2)",
          borderColor: "rgba(75, 192, 192, 1)",
          borderWidth: 1,
        },
        {
          label: "GRASP",
          data: outputMaxValuesGRASP.map((result) => result.value),
          backgroundColor: "rgba(255, 99, 132, 0.2)",
          borderColor: "rgba(255, 99, 132, 1)",
          borderWidth: 1,
        },
      ],
    },
    options: {
      scales: {
        y: {
          beginAtZero: true,
        },
      },
    },
  });

  return canvas.toBuffer("image/png");
}

function generateTimeComparisonChart(
  outputMaxValuesDynamic,
  outputMaxValuesGRASP
) {
  const canvas = createCanvas(600, 400);
  const ctx = canvas.getContext("2d");

  const chart = new Chart(ctx, {
    type: "bar",
    data: {
      labels: [...Array(16).keys()].map((i) => `Test ${i + 1}`),
      datasets: [
        {
          label: "Dynamic Programming",
          data: outputMaxValuesDynamic.map((result) => result.time),
          backgroundColor: "rgba(75, 192, 192, 0.2)",
          borderColor: "rgba(75, 192, 192, 1)",
          borderWidth: 1,
        },
        {
          label: "GRASP",
          data: outputMaxValuesGRASP.map((result) => result.time),
          backgroundColor: "rgba(255, 99, 132, 0.2)",
          borderColor: "rgba(255, 99, 132, 1)",
          borderWidth: 1,
        },
      ],
    },
    options: {
      scales: {
        y: {
          beginAtZero: true,
        },
      },
    },
  });

  return canvas.toBuffer("image/png");
}

function calculateStatistics(results) {
  const values = results.map((result) => result.value);
  const times = results.map((result) => result.time);

  const meanValue =
    values.reduce((sum, value) => sum + value, 0) / values.length;
  const stdDevValue = Math.sqrt(
    values.reduce((sum, value) => sum + (value - meanValue) ** 2, 0) /
      (values.length - 1)
  );

  const meanTime = times.reduce((sum, time) => sum + time, 0) / times.length;
  const stdDevTime = Math.sqrt(
    times.reduce((sum, time) => sum + (time - meanTime) ** 2, 0) /
      (times.length - 1)
  );

  return {
    meanValue,
    stdDevValue,
    meanTime,
    stdDevTime,
  };
}

async function main() {
  const maxIterations = 100;
  const alpha = 0.3;
  const outputMaxValuesDynamic = [];
  const outputMaxValuesGRASP = [];

  for (let iterator = 1; iterator <= 16; iterator++) {
    const inputFilePath = `input/input${iterator}.in`;
    const inputData = fs.readFileSync(inputFilePath, "utf8").split("\n");
    const n = parseInt(inputData[0]);
    const capacity = parseInt(inputData[n + 1]);
    const value = [];
    const weight = [];

    for (let i = 1; i <= n; i++) {
      const [_, v, w] = inputData[i].match(/\d+/g).map(Number);
      value.push(v);
      weight.push(w);
    }

    const dynamicResult = knapsackDynamic(value, weight, capacity);
    outputMaxValuesDynamic.push(dynamicResult);

    const graspResult = graspKnapsack(
      value,
      weight,
      capacity,
      maxIterations,
      alpha
    );
    const dynamicTime = dynamicResult.time;
    const graspTime = graspResult.time;
    const differencePercentageTime =
      ((graspTime - dynamicTime) / dynamicTime) * 100 * -1;

    console.log(`Teste ${iterator}
      Valor Dinâmico = ${dynamicResult.value}, Valor GRASP = ${
      graspResult.value
    },
      Tempo dinâmico: ${dynamicTime.toFixed(
        2
      )} ms, Tempo GRASP: ${graspTime.toFixed(
      2
    )} ms, GRASP eficiência em tempo: ${differencePercentageTime.toFixed(2)}%
      `);

    outputMaxValuesGRASP.push(graspResult);
  }

  const dynamicStats = calculateStatistics(outputMaxValuesDynamic);
  const graspStats = calculateStatistics(outputMaxValuesGRASP);

  console.log(`Estátisticas dinâmico:
    Valor médio: ${dynamicStats.meanValue.toFixed(2)}, 
    Desvio Padrão do valor: ${dynamicStats.stdDevValue.toFixed(2)}, 
    Tempo médio: ${dynamicStats.meanTime.toFixed(2)} ms, 
    Desvio padrão do tempo: ${dynamicStats.stdDevTime.toFixed(2)} ms \n`);

  console.log(`Estátisticas GRASP:
    Valor médio: ${graspStats.meanValue.toFixed(2)}, 
    Desvio Padrão do valor: ${graspStats.stdDevValue.toFixed(2)}, 
    Tempo médio: ${graspStats.meanTime.toFixed(2)} ms, 
    Desvio padrão do tempo: ${graspStats.stdDevTime.toFixed(2)} ms \n`);

  console.log(`Conclusão:
  GRASP é ${(
    100 -
    (graspStats.meanValue / dynamicStats.meanValue) * 100
  ).toFixed(2)}% menos eficiente que o algoritmo dinâmico.
  Porém, GRASP é ${(
    (dynamicStats.meanTime / graspStats.meanTime) *
    100
  ).toFixed(2)}% mais rápido que o algoritmo dinâmico.`);

  const valueComparisonChartBuffer = generateValueComparisonChart(
    outputMaxValuesDynamic,
    outputMaxValuesGRASP
  );

  const timeComparisonChartBuffer = generateTimeComparisonChart(
    outputMaxValuesDynamic,
    outputMaxValuesGRASP
  );

  fs.writeFileSync("valueComparisonChart.png", valueComparisonChartBuffer);
  fs.writeFileSync("timeComparisonChart.png", timeComparisonChartBuffer);
}

main();
